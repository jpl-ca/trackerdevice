<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function getIndex(Request $request)
    {
        $data = $request->all();

        foreach ($data as $key => $value) {
            \Log::info("key: " . $key . ", value: " . $value);
        }

        return "OK";
    }
}
